{
    "_id": "6XlGTt3RveX49YbC",
    "data": {
        "abilities": {
            "cha": {
                "mod": -1
            },
            "con": {
                "mod": 5
            },
            "dex": {
                "mod": 1
            },
            "int": {
                "mod": -2
            },
            "str": {
                "mod": 6
            },
            "wis": {
                "mod": 2
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 22
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "",
                "max": 120,
                "temp": 0,
                "value": 120
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 12
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [
                    {
                        "type": "Swim",
                        "value": "20 feet"
                    }
                ],
                "value": "20 feet"
            }
        },
        "details": {
            "alignment": {
                "value": "N"
            },
            "creatureType": "Plant",
            "flavorText": "<p>Shamblers are cunning carnivorous plants that resemble heaps of wet, rotting vegetation. Even when standing erect on their stumpy legs, shamblers don't have much in the way of identifiable anatomy; they are tangles of parasitic vines that lash out at prey with their longest creepers, entwined together to deliver powerful blows. Sometimes called \"shambling mounds,\" these ambush predators have a particular fondness for flesh, and are most well known for their ability to hunker down and hide in plain sight. A shambler can draw nutrients from plant matter or soil, but won't do so if it suspects meat might soon wander near, and indeed may lie in wait for days in anticipation of such a meal.<br /><br /> Shamblers are usually solitary creatures. Fierce thunderstorms are among the few events that bring numerous shamblers together. During such gatherings, the plants gather around strange but seemingly sacred earthen mounds in remote jungles and swamps and cavort and caper excitedly, stretching their tendrils toward the sky to hope for-or perhaps to summon-intense lightning strikes. Not only are these strange plants unharmed by even the most powerful electrical strikes, but they seem to become supernaturally empowered by such bursts of raw energy. Shamblers consider being struck by lightning a sign of divine favor, and those shamblers frequently blasted by bolts of electricity become respected  elders among their kind. Shambler elders are few and far between and may possess a wide array of abilities; some can cast primal spells, others can empathically call upon the aid of other shamblers in the area, and still others wield awesome powers over electricity.<br /><br /> Other types of shamblers exist, including those composed not of plant matter but fungal growth. These creatures, known as \"spore mounds,\" discharge clouds of toxic spores from their bodies when struck in combat. In semiarid deserts, shambler-like plants called tanglethorns dwell amid sagebrush and rock formations; they resemble bunches of tumbleweeds and cacti and inflict deep, bloody lacerations with their attacks.</p>",
            "level": {
                "value": 6
            },
            "source": {
                "value": "Pathfinder Bestiary"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 17
            },
            "reflex": {
                "saveDetail": "",
                "value": 11
            },
            "will": {
                "saveDetail": "",
                "value": 14
            }
        },
        "traits": {
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "electricity"
                ]
            },
            "dr": [
                {
                    "label": "Fire",
                    "type": "fire",
                    "value": "5"
                }
            ],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": [
                    "common",
                    "elven",
                    "sylvan",
                    "custom"
                ]
            },
            "rarity": {
                "value": "common"
            },
            "senses": {
                "value": "darkvision"
            },
            "size": {
                "value": "lg"
            },
            "traits": {
                "custom": "",
                "value": [
                    "plant"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "EUf1gqfe5un9Aprz",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "value": [
                        "Grab"
                    ]
                },
                "bonus": {
                    "value": 17
                },
                "damageRolls": {
                    "rsrxo3cheibra8yjrb66": {
                        "damage": "2d8+8",
                        "damageType": "bludgeoning"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "reach-10"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Vine",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "cr8chy1goN1ZSrnT",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>When it's not in danger, the shambler spends 1 minute settling into a pile that looks like a lump of loose vegetation. While it is in this form, creatures must actively Seek and succeed at a <span data-pf2-check=\"perception\" data-pf2-dc=\"22\" data-pf2-label=\"Mound DC\" data-pf2-show-dc=\"gm\">Perception</span> check (<span data-pf2-check=\"perception\" data-pf2-dc=\"28\" data-pf2-label=\"Mound DC\" data-pf2-show-dc=\"gm\">Perception</span> in forests or swamps) to detect the shambler's true nature.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Mound",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "HjqV5crA9cRbpUVj",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "reaction"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>Whenever the shambler would take electricity damage or is targeted with an electricity effect, it gains 12 temporary HP and is quickened until the end of its next turn. It can use its extra action to Stride, Strike, or Swim. <strong>Shamble</strong><strong>Requirements</strong> The shambler is in mound form. <strong>Trigger</strong> A creature unaware of the shambler's true nature comes within 10 feet. <strong>Effect</strong> The shambler makes a vine Strike against the creature. Then roll initiative.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/mystery-man.svg",
            "name": "Electric Surge",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "Jn3EbZv9d1JIQuz9",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 1
                },
                "description": {
                    "value": "<p><strong>Requirements</strong> The monster's last action was a success with a Strike that lists Grab in its damage entry, or it has a creature grabbed using this action. <strong>Effect</strong> The monster automatically Grabs the target until the end of the monster's next turn. The creature is grabbed by whichever body part the monster attacked with, and that body part can't be used to Strike creatures until the grab is ended. Using Grab extends the duration of the monster's Grab until the end of its next turn for all creatures grabbed by it. A grabbed creature can use the Escape action to get out of the grab, and the Grab ends for a grabbed creatures if the monster moves away from it.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/mystery-man.svg",
            "name": "Grab",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "PfK4YgueBXw7cjqo",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 2
                },
                "description": {
                    "value": "<p>The shambler makes a vine Strike against each creature within reach. Its multiple attack penalty increases only after all the attacks.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/mystery-man.svg",
            "name": "Vine Lash",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "WOIo42ImBVjVcGRY",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 16
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Athletics",
            "sort": 600000,
            "type": "lore"
        },
        {
            "_id": "xAxV9yRmp780wKql",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 12
                },
                "proficient": {
                    "value": 0
                },
                "rules": [
                    {
                        "key": "PF2E.RuleElement.FlatModifier",
                        "label": "In Water",
                        "predicate": {
                            "any": [
                                "terrain:forest",
                                "terrain:swamp"
                            ]
                        },
                        "selector": "stealth",
                        "value": 6
                    }
                ],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "variants": {
                    "0": {
                        "label": "+18 in forests or swamps",
                        "options": "terrain:forest, terrain:swamp"
                    }
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Stealth",
            "sort": 700000,
            "type": "lore"
        }
    ],
    "name": "Shambler",
    "token": {
        "disposition": -1,
        "height": 2,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Shambler",
        "width": 2
    },
    "type": "npc"
}
