{
    "_id": "2GRPw4VK6zfCS2Qw",
    "data": {
        "abilities": {
            "cha": {
                "mod": -2
            },
            "con": {
                "mod": 0
            },
            "dex": {
                "mod": 3
            },
            "int": {
                "mod": 0
            },
            "str": {
                "mod": -1
            },
            "wis": {
                "mod": 1
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 17
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "",
                "max": 17,
                "temp": 0,
                "value": 17
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 3
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [
                    {
                        "type": "Fly",
                        "value": "40 feet"
                    }
                ],
                "value": "15 feet"
            }
        },
        "details": {
            "alignment": {
                "value": "N"
            },
            "creatureType": "Construct",
            "flavorText": "<p>A homunculus is a tiny servitor construct created by a crafter to serve as a spy, scout, messenger, or assistant. When a crafter first begins to study the art of creating constructs, she often crafts a homunculus first, since the creation process is simple and inexpensive due to a magical shortcut: the use of the creator's own blood. This forges a link between the homunculus and its master, causing the homunculus to gain a spark of the creator's intellect, as well as the same moral values and some of the creator's basic personality traits. Homunculi left to their own devices never stray far from their masters.<br /><br /> In most cases, a homunculus doesn't survive the death of its master for long. Deprived of its creator, a homunculus loses focus and grows increasingly self-destructive, and some even end up battering themselves to destruction. Rarely, a homunculus with a slain master survives the trauma with its mind intact, often seeing itself as its deceased creator's child or successor and attempting to further its creator's legacy as best it can. In such cases, and if the homunculus was in close proximity to its master upon that creature's death, a portion of the dead master's soul \"infects\" the surviving homunculus as it passes on to the afterlife. This doesn't result in a truly soulbound homunculus (see sidebar), since only a fragment of the soul is left behind, but this is still enough to grant the homunculus a greater personality, free will of its own, and perhaps most importantly, the ability to speak. Over time, a few of these \"awakened\" homunculi even go so far as to become convinced that they are the reincarnation of their prior masters, although their actual personalities never quite reach the depth and complexity of a truly living creature. They are, at best, caricatures of the master, and at worst they become awful, bitter-minded parodies of life itself. Still, a free-willed homunculus might pursue studies in its creator's class, becoming a unique creature with the abilities of that class if time and fortune permit.<br /><br /> Homunculi are created from a mixture of clay, ash, mandrake root, spring water, and a pint of the creator's own blood. It is possible for a separate donor to provide the blood, but the process is more difficult.</p>",
            "level": {
                "value": 0
            },
            "source": {
                "value": "Pathfinder Bestiary"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 2
            },
            "reflex": {
                "saveDetail": "",
                "value": 7
            },
            "will": {
                "saveDetail": "",
                "value": 3
            }
        },
        "traits": {
            "ci": [],
            "di": {
                "custom": "",
                "value": [
                    "bleed",
                    "death effects",
                    "disease",
                    "doomed",
                    "drained",
                    "fatigued",
                    "healing",
                    "necromancy",
                    "nonlethal attacks",
                    "paralyzed",
                    "poison",
                    "sickened",
                    "unconscious"
                ]
            },
            "dr": [],
            "dv": [],
            "languages": {
                "custom": ", master link",
                "selected": [],
                "value": [
                    "common",
                    "custom"
                ]
            },
            "rarity": {
                "value": "common"
            },
            "senses": {
                "value": "darkvision"
            },
            "size": {
                "value": "tiny"
            },
            "traits": {
                "custom": "",
                "value": [
                    "construct"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "ICjSF2djJxtdnASi",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "value": [
                        "homunculus poison"
                    ]
                },
                "bonus": {
                    "value": 7
                },
                "damageRolls": {
                    "49yt97hqjo70bke63nxy": {
                        "damage": "1d4",
                        "damageType": "piercing"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "finesse",
                        "magical"
                    ]
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Jaws",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "WLKYWXEiwmc9uCr5",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A homunculus can't speak, but it is telepathically linked to its creator. It can share information back and forth, including its master's knowledge and everything the homunculus hears. The range of this link is 1,500 feet. The homunculus adopts the same alignment as its creator and is utterly faithful. If the homunculus is destroyed, the master takes [[/r 2d10 #mental]]{2d10 mental damage}. If the master is slain, the homunculus becomes mindless, claims its current location as its lair, and instinctively attacks anyone who comes near.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "arcane",
                        "divination",
                        "mental"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/mystery-man.svg",
            "name": "Master Link",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "WhTbPOtlxlYvCpJ6",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A homunculus has one dose of poison in a reservoir in its head. It can refill this poison from its reserves with an Interact action. <strong>Saving Throw</strong> <span data-pf2-check=\"fortitude\" data-pf2-dc=\"15\" data-pf2-traits=\"poison\" data-pf2-label=\"Homunculus Poison DC\" data-pf2-show-dc=\"gm\">Fortitude</span>; <strong>Maximum Duration</strong> 6 rounds; <strong>Stage 1</strong> 1d6 poison and @Compendium[pf2e.conditionitems.Enfeebled]{Enfeebled 1}</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "poison"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/mystery-man.svg",
            "name": "Homunculus Poison",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "Rn5hk6K68uRSgE8C",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 5
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Acrobatics",
            "sort": 400000,
            "type": "lore"
        },
        {
            "_id": "iFBf5O4rRRlAEdFg",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 5
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Stealth",
            "sort": 500000,
            "type": "lore"
        }
    ],
    "name": "Homunculus",
    "token": {
        "disposition": -1,
        "height": 0.5,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Homunculus",
        "width": 0.5
    },
    "type": "npc"
}
