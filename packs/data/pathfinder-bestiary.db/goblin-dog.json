{
    "_id": "0rfropeocJWXC6pg",
    "data": {
        "abilities": {
            "cha": {
                "mod": -1
            },
            "con": {
                "mod": 2
            },
            "dex": {
                "mod": 2
            },
            "int": {
                "mod": -4
            },
            "str": {
                "mod": 3
            },
            "wis": {
                "mod": 1
            }
        },
        "attributes": {
            "ac": {
                "details": "",
                "value": 17
            },
            "allSaves": {
                "value": ""
            },
            "hp": {
                "details": "",
                "max": 17,
                "temp": 0,
                "value": 17
            },
            "initiative": {
                "ability": "perception"
            },
            "perception": {
                "value": 6
            },
            "shield": {
                "ac": 0,
                "brokenThreshold": 0,
                "hardness": 0,
                "max": 0,
                "value": 0
            },
            "speed": {
                "otherSpeeds": [],
                "value": "40"
            }
        },
        "details": {
            "alignment": {
                "value": "N"
            },
            "creatureType": "Animal",
            "flavorText": "<p>Goblins' eponymous pets aren't true canines at all, but rather large, blunt-nosed rodents with thin bodies and long legs. As cowardly as they are ugly, goblin dogs prefer to lurk behind bushes or in deep shadows, pouncing upon lone or wounded prey. Goblin dogs frequently lair and roam in packs, but they are likely to flee from a fight if injured, even if it means abandoning their pack-mates.</p>\n<p>Goblin dogs take their name from their long association with goblins, who breed the beasts as guard animals and mounts. Most goblins take issue with the name, as the average goblin is appalled at the suggestion that these, their favored mounts, have anything at all to do with actual dogs. Of course, being goblins, they haven't bothered to come up with their own, unique name for goblin dogs.</p>\n<p>Even the most pampered goblin dogs have itchy mange and prolific dander that tenaciously affects those who come in contact with them. This \"goblin pox\" causes itchy hives and festering sores that are as unsightly as they are irritating and distracting. Goblin dog dander causes allergic reactions in nearly all other creatures that don't share the goblin dogs' terrible hygiene-with the notable exception, of course, of goblins, who remain entirely immune to the disease regardless of cleanliness.</p>\n<p>Hunger can drive goblin dogs to bouts of uncharacteristic violence; many goblins purposefully starve their pets to make them more aggressive in battle. Goblin dogs subsist on whatever organic material they can scavenge, and they particularly enjoy fresh carrion. Although goblins are far from picky eaters, they value goblin dogs as pets because the noisome animals will consume food that even goblins won't dare to eat. \"Will it eat?\" is one of several games goblins enjoy playing with their goblin dog pets, where a wide range of mouth-sized morsels (not always edible or safe to consume) are dangled before a goblin dog's snout. \"Will it die?\" is often a game played after \"Will it eat?\" Goblin dogs who survive the second game earn renown for their digestive prowess and often become favored tribal pets treated better than most of the rank-and-file goblins.</p>",
            "level": {
                "value": 1
            },
            "source": {
                "value": "Pathfinder Bestiary"
            }
        },
        "resources": {},
        "saves": {
            "fortitude": {
                "saveDetail": "",
                "value": 8
            },
            "reflex": {
                "saveDetail": "",
                "value": 8
            },
            "will": {
                "saveDetail": "",
                "value": 5
            }
        },
        "traits": {
            "attitude": {
                "value": "hostile"
            },
            "ci": [],
            "di": {
                "custom": "",
                "value": []
            },
            "dr": [],
            "dv": [],
            "languages": {
                "custom": "",
                "selected": [],
                "value": []
            },
            "rarity": {
                "value": "common"
            },
            "senses": {
                "value": "low-light vision, scent (imprecise) 30 feet"
            },
            "size": {
                "value": "med"
            },
            "traits": {
                "custom": "",
                "value": [
                    "animal"
                ]
            }
        }
    },
    "effects": [],
    "flags": {},
    "img": "systems/pf2e/icons/default-icons/npc.svg",
    "items": [
        {
            "_id": "FrlYBDNV4RyfqLaY",
            "data": {
                "attack": {
                    "value": ""
                },
                "attackEffects": {
                    "custom": "",
                    "value": [
                        "goblin-pox"
                    ]
                },
                "bonus": {
                    "value": 9
                },
                "damageRolls": {
                    "j70yxoepbt0wfo8tuuta": {
                        "damage": "1d6+3",
                        "damageType": "piercing"
                    }
                },
                "description": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "weaponType": {
                    "value": "melee"
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/melee.svg",
            "name": "Jaws",
            "sort": 100000,
            "type": "melee"
        },
        {
            "_id": "xRQZegRo2JOZB7hU",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>The monster can see in dim light as though it were bright light, so it ignores the @Compendium[pf2e.conditionitems.Concealed]{Concealed} condition due to dim light.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "low-light-vision",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.fFu2sZz4KB01fVRc"
                }
            },
            "img": "systems/pf2e/icons/default-icons/action.svg",
            "name": "Low-Light Vision",
            "sort": 200000,
            "type": "action"
        },
        {
            "_id": "4BNXIgVORiBDrL95",
            "data": {
                "actionCategory": {
                    "value": "interaction"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>Scent involves sensing creatures or objects by smell, and is usually a vague sense. The range is listed in the ability, and it functions only if the creature or object being detected emits an aroma (for instance, incorporeal creatures usually do not exude an aroma).</p>\n<p>If a creature emits a heavy aroma or is upwind, the GM can double or even triple the range of scent abilities used to detect that creature, and the GM can reduce the range if a creature is downwind.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "scent",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.rqfnQ5VHT5hxm25r"
                }
            },
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Scent (Imprecise) 30 feet",
            "sort": 300000,
            "type": "action"
        },
        {
            "_id": "M1Uh0E4VLZObTo3I",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "reaction"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p><span data-pf2-check=\"reflex\" data-pf2-label=\"Buck DC\" data-pf2-dc=\"17\" data-pf2-show-dc=\"gm\">basic Reflex</span></p>\n<hr />\n<p>Most monsters that serve as mounts can attempt to buck off unwanted or annoying riders, but most mounts will not use this reaction against a trusted creature unless the mounts are spooked or mistreated.</p>\n<p><strong>Trigger</strong> A creature @Compendium[pf2e.actionspf2e.Mount]{Mounts} or uses the @Compendium[pf2e.actionspf2e.Command an Animal]{Command an Animal} action while riding the monster.</p>\n<p><strong>Effect</strong> The triggering creature must succeed at a Reflex saving throw against the listed DC or fall off the creature and land @Compendium[pf2e.conditionitems.Prone]{Prone}. If the save is a critical failure, the triggering creature also takes [[/roll 1d6 #bludgeoning]] bludgeoning damage in addition to the normal damage for the fall.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": "buck",
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {
                "core": {
                    "sourceId": "Compendium.pf2e.bestiary-ability-glossary-srd.nZMQh4AaBr291TUf"
                }
            },
            "img": "systems/pf2e/icons/actions/Reaction.webp",
            "name": "Buck",
            "sort": 400000,
            "type": "action"
        },
        {
            "_id": "WU4cXLzhnoCczalO",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p>A creature that hits the goblin dog with an unarmed attack, tries to @Compendium[pf2e.actionspf2e.Grapple]{Grapple} it, or otherwise touches it is exposed to goblin pox.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Irritating Dander",
            "sort": 500000,
            "type": "action"
        },
        {
            "_id": "OWImE1DpRm2byzUX",
            "data": {
                "actionCategory": {
                    "value": "defensive"
                },
                "actionType": {
                    "value": "reaction"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p><strong>Requirement</strong> A creature must be mounted on the goblin dog.</p>\n<p><strong>Trigger</strong> The rider issues a command to the goblin dog.</p>\n<p><strong>Effect</strong> The goblin dog @Compendium[pf2e.actionspf2e.Step]{Steps} before following the command.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Reaction.webp",
            "name": "Juke",
            "sort": 600000,
            "type": "action"
        },
        {
            "_id": "1AbBtZhPY7lYpSm0",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "passive"
                },
                "actions": {
                    "value": null
                },
                "description": {
                    "value": "<p><em data-visibility=\"gm\">Goblins and goblin dogs are immune to goblin pox.</em></p>\n<p><strong>Saving Throw</strong> <span data-pf2-check=\"fortitude\" data-pf2-traits=\"disease\" data-pf2-label=\"Goblin Pox DC\" data-pf2-dc=\"17\" data-pf2-show-dc=\"gm\">Fortitude</span>;</p>\n<p><em data-visibility=\"gm\"><strong>Stage 1</strong> @Compendium[pf2e.conditionitems.Sickened]{Sickened 1} (1 round);</em></p>\n<p><em data-visibility=\"gm\"><strong>Stage 2</strong> @Compendium[pf2e.conditionitems.Sickened]{Sickened 1} and @Compendium[pf2e.conditionitems.Slowed]{Slowed 1} (1 round);</em></p>\n<p><em data-visibility=\"gm\"><strong>Stage 3</strong> @Compendium[pf2e.conditionitems.Sickened]{Sickened 2} and can't reduce its sickened value below 1 (1 day).</em></p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "disease"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/Passive.webp",
            "name": "Goblin Pox",
            "sort": 700000,
            "type": "action"
        },
        {
            "_id": "oEnTmvBRzYBAWuou",
            "data": {
                "actionCategory": {
                    "value": "offensive"
                },
                "actionType": {
                    "value": "action"
                },
                "actions": {
                    "value": 2
                },
                "description": {
                    "value": "<p>The goblin dog vigorously scratches itself, exposing all adjacent creatures to goblin pox.</p>"
                },
                "requirements": {
                    "value": ""
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": [
                        "manipulate"
                    ]
                },
                "trigger": {
                    "value": ""
                },
                "weapon": {
                    "value": ""
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/actions/TwoActions.webp",
            "name": "Scratch",
            "sort": 800000,
            "type": "action"
        },
        {
            "_id": "6f4wOHPtCdDTXBoi",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 6
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Athletics",
            "sort": 900000,
            "type": "lore"
        },
        {
            "_id": "DDTEebmrqDGCJbuX",
            "data": {
                "description": {
                    "value": ""
                },
                "item": {
                    "value": 0
                },
                "mod": {
                    "value": 7
                },
                "proficient": {
                    "value": 0
                },
                "rules": [],
                "slug": null,
                "source": {
                    "value": ""
                },
                "traits": {
                    "custom": "",
                    "rarity": {
                        "value": "common"
                    },
                    "value": []
                }
            },
            "effects": [],
            "flags": {},
            "img": "systems/pf2e/icons/default-icons/lore.svg",
            "name": "Stealth",
            "sort": 1000000,
            "type": "lore"
        }
    ],
    "name": "Goblin Dog",
    "token": {
        "disposition": -1,
        "height": 1,
        "img": "systems/pf2e/icons/default-icons/npc.svg",
        "name": "Goblin Dog",
        "width": 1
    },
    "type": "npc"
}
